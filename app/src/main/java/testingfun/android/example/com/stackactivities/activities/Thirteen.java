package testingfun.android.example.com.stackactivities.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

import testingfun.android.example.com.stackactivities.R;

/**
 * Created by andrii on 08.08.15.
 */
public class Thirteen extends Activity {
    public static final String COUNT_FROM_START_ACTIVITY = "COUNT_FROM_START_ACTIVITY";
    public static final String COUNT_FROM_SAVE_INSTANCE = "COUNT_FROM_SAVE_INSTANCE";
    private int mCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("TAG", "ReopenedActivity onCreate(). Bundle" + savedInstanceState);

        mCount = getIntent().getExtras().getInt(COUNT_FROM_START_ACTIVITY);

        Bundle bundle = new Bundle();
        bundle.putInt(COUNT_FROM_START_ACTIVITY, mCount);

//        ViewPagerFragment fragment = new ViewPagerFragment();
//        fragment.setArguments(bundle);
//        getFragmentManager().beginTransaction().replace(android.R.id.content, fragment).commit();
        setContentView(R.layout.reopened_fragment);
        ((TextView) findViewById(R.id.show_count)).setText("Count: " + mCount);
        findViewById(R.id.start_another_activity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Thirteen.class);
                i.putExtra(Thirteen.COUNT_FROM_START_ACTIVITY, ++mCount);
                startActivity(i);
            }
        });
        LinearLayout post = (LinearLayout) findViewById(R.id.container);
        for (int i = 0; i < 5; i++) {
            ImageView imageView = new ImageView(getApplicationContext());
            try {
                InputStream is = getApplicationContext().getAssets().open("bullet.jpg");
                Drawable d = Drawable.createFromStream(is, null);
                imageView.setImageDrawable(d);
            } catch (IOException e) {
                e.printStackTrace();
            }
            post.addView(imageView);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("TAG", "ReopenedActivity onStart()");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mCount = savedInstanceState.getInt(COUNT_FROM_SAVE_INSTANCE);
        Log.d("TAG", "ReopenedActivity onRestoreInstanceState()");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(COUNT_FROM_SAVE_INSTANCE, mCount);
        Log.d("TAG", "ReopenedActivity onSaveInstanceState()");
    }

    @Override
    protected void onStop() {
        Log.d("TAG", "ReopenedActivity onStop()");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d("TAG", "ReopenedActivity onDestroy()");
        super.onDestroy();
    }
}
