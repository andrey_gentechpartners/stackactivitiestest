package testingfun.android.example.com.stackactivities;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by andrii on 08.08.15.
 */
public class ViewPagerFragment extends Fragment {
    private int mCount;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCount = getArguments().getInt(ReopenedActivity.COUNT_FROM_START_ACTIVITY);
        Log.d("TAG", "ViewPager onCreate(). Bundle + " + savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.reopened_fragment, container, false);
        ((TextView) view.findViewById(R.id.show_count)).setText("Count: " + mCount);
        view.findViewById(R.id.start_another_activity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ReopenedActivity.class);
                i.putExtra(ReopenedActivity.COUNT_FROM_START_ACTIVITY, ++mCount);
                startActivity(i);
            }
        });
        LinearLayout post = (LinearLayout) view.findViewById(R.id.container);
        for (int i = 0; i < 5; i++) {
            ImageView imageView = new ImageView(getActivity().getApplicationContext());
            try {
                InputStream is = getActivity().getApplicationContext().getAssets().open("bullet.jpg");
                Drawable d = Drawable.createFromStream(is, null);
                imageView.setImageDrawable(d);
            } catch (IOException e) {
                e.printStackTrace();
            }
            post.addView(imageView);
        }
        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("TAG", "ViewPager onStop()");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("TAG", "ViewPager onSaveInstanceState()");
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.d("TAG", "ViewPager onViewStateRestored()");
    }

    @Override
    public void onDestroy() {
        Log.d("TAG", "ViewPager onDestroy()");
        super.onDestroy();
    }
}
