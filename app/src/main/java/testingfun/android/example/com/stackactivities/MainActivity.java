package testingfun.android.example.com.stackactivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import testingfun.android.example.com.stackactivities.activities.One;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.launch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ReopenedActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra(ReopenedActivity.COUNT_FROM_START_ACTIVITY, 1);
                startActivity(i);
            }
        });

        findViewById(R.id.start_test_2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), One.class);
                i.putExtra(ReopenedActivity.COUNT_FROM_START_ACTIVITY, 1);
                startActivity(i);
            }
        });
        Log.d("TAG", "Main onCreate()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("TAG", "Main onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("TAG", "Main onResume()");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d("TAG", "Main onRestoreInstanceState()");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d("TAG", "Main onSaveInstanceState()");
        super.onSaveInstanceState(outState);
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.d("TAG", "Main onPause()");
    }

    @Override
    protected void onStop() {
        Log.d("TAG", "Main onstop()");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d("TAG", "Main onDestroy()");
        super.onDestroy();
    }
}